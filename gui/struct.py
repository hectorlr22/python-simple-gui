"""
Module that contains all components that define the GUI structure.
"""
VERTICAL = "vertical"
HORIZONTAL = "horizontal"


class Struct(object):
    """
    Base structure to define the basic behaviour and basic design.
    """
    width = None
    height = None


class Layout(Struct):
    """
    Define a structure that put elements together in line, vertically or
    horizontally.
    """
    orientation = VERTICAL

import xml.etree.ElementTree as ET

from . import struct, utils, widgets


class Factory(object):
    tree = None

    def __init__(self, xml_file):
        self.tree = ET.parse(xml_file)

    def _element_to_label(self, element):
        label = widgets.Label()
        label.text = element.get("text")
        label.color = element.get("color")
        label.background = element.get("background")
        return label

    def _element_to_text(self, element):
        text = widgets.Text()
        text.background = element.get("background")
        text.color = element.get("color")
        text.mask = element.get("mask")
        return text

    def _element_to_button(self, element):
        button = widgets.Button()
        button.background = element.get("background")
        button.color = element.get("color")
        button.text = element.get("text")
        return button

    def convert(self, element, expected):
        utils.raise_type_if_needed(ET.Element, element)

        if expected is widgets.Label:
            return self._element_to_label(element)
        elif expected == widgets.Text:
            return self._element_to_text(element)
        elif expected == widgets.Button:
            return self._element_to_button(element)
        return None

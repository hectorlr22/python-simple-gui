"""
Module that contains all the widgets that can be represented in the graphical
interface.
"""


class Widget(object):
    """
    General widget to represent a graphical component with the base behaviour
    """
    id = None
    height = None
    width = None


class Label(Widget):
    """
    Represents a text which is displayed to user and it's read-only
    """
    text = ""
    color = None
    background = None

    def __init__(self, text=None):
        self.text = text


class Text(Widget):
    """
    Represents a text that can be modified by the user
    """
    background = None
    color = None
    mask = None


class Button(Widget):
    """
    Represents a clickable graphical component
    """
    background = None
    color = None
    text = None

def raise_type_if_needed(clazz, instance):
    if not isinstance(instance, clazz):
        raise TypeError(
            "The element should be instance of {} instead of {}".format(
                clazz.__name__,
                type(instance)
            )
        )
